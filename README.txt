



API

hook_uc_license_keys_import

return: array of callback => label

example:

  return array(
    'my_module_callback' => t('My Module Callback'),
  );
