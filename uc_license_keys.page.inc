<?php

/**
 * @file
 *   page callback functions for uc_license_keys
 *
 */

/**
 * uc_license_keys_add
 * @param $form
 * @return $form
 */
function uc_license_keys_add($form) {

	if (empty($form['storage'])) {
		$form = _uc_license_keys_add_form();
	}
	else {
		$form = call_user_func($form['storage']['method'], $form['storage']['sku']);

		$form['back'] = array(
			'#type' => 'button',
			'#value' => t('Back'),
			'#submit' => array('uc_license_keys_add_back'),
		);
	}

	return $form;
}

/** 
 * _uc_license_keys_add_form
 * @return $form a drupal form
 */
function _uc_license_keys_add_form() {
	$methods = module_invoke_all('uc_license_keys_import');
	$products = _uc_license_keys_load_products();	

	if (count($products) <= 0) {
		drupal_set_message(t('No products have been configured. Please configure a product for your store before continuing.'), 'error');
		return;
	}

	$method_options = array();
	foreach($methods as $cb => $name) {
		$method_options[$cb] = $name;
	}

	$product_options = array();
	foreach($products as $product) {
		$product_options[$product->model] = $product->title . ' (' . $product->model . ')';
	}

	$form['sku'] = array(
		'#type' => 'select',
		'#title' => t('Product'),
		'#description' => t('Select the Product (SKU) that you wish to use.'),
		'#options' => $product_options,
		'#required' => FALSE,
	);

	// should always be one options, the default
	$form['method'] = array(
		'#type' => 'select',
		'#title' => t('Import Method'),
		'#description' => t('Select the import method to use.'),
		'#options' => $method_options,
		'#default_value' => 'uc_license_keys_import_form',
		'#required' => TRUE,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#submit' => array('uc_license_keys_add_back'),
		'#value' => t('Next'),
	);

	return $form;
}

/**
 * uc_license_keys_add_back
 * @param $form
 * @param $form_state
 */
function uc_license_keys_add_back($form, &$form_state) {
	if (empty($form_state['storage'])) {
		$form_state['storage']['method'] = $form_state['values']['method'];
		$form_state['storage']['sku'] = $form_state['values']['sku'];
		$form_state['rebuild'] = TRUE;
	}
}

/**
 * uc_license_keys_settings
 * @param $form_state
 * @return $form
 */
function uc_license_keys_settings($form_state) {
	$form['notification'] = array(
		'#type' => 'fieldset',
		'#title' => t('Notification Settings'),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
	);
/*	$form['notification']['uc_license_keys_notification'] = array(
		'#type' => 'radios',
		'#title' => t('Notification Enabled'),
		'#description' => t('Enable or disable the notification of license keys.'),
		'#options' => array(0 => t('Enabled'), 1 => t('Disabled')),
		'#default_value' => variable_get('uc_license_keys_notification', 1),
		'#required' => TRUE,
	); */

	$statuses = array();
	foreach (uc_order_status_list('general') as $status) {
		$statuses[$status['id']] = $status['title'];
	}
	$form['uc_license_keys_order_status'] = array(
		'#type' => 'select',
		'#title' => t('Order status'),
		'#default_value' => variable_get('uc_license_keys_order_status','completed'),
		'#description' => t('Where in the order status the user will be notified of the license keys.'),
		'#options' => $statuses,
	);
	$form['notification']['uc_license_keys_notification_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Notification Subject'),
		'#description' => t('The subject of the message to be sent to the user. !help', array('!help' => l(t('Uses Order and License key tokens'), 'admin/store/help/tokens'))),
		'#default_value' => variable_get('uc_license_keys_notification_subject', uc_get_message('uc_license_keys_subject')),
	);
	$form['notification']['uc_license_keys_notification_message'] = array(
		'#type' => 'textarea',
		'#title' => t('Notification Message'),
		'#description' => t('The message the user receives after completing an order with license keys. !help', array('!help' => l(t('Uses Order and License key tokens'), 'admin/store/help/tokens'))),
		'#default_value' => variable_get('uc_license_keys_notification_message', uc_get_message('uc_license_keys_message')),
		'#rows' => 10,
	);
/*	$form['notification']['uc_license_keys_notification_format'] = filter_form(variable_get('uc_license_keys_notification_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_license_keys_notification_format')); */
	$form['notification']['uc_license_keys_notification_recipients'] = array(
		'#type' => 'textarea',
		'#title' => t('Notification Recipients'),
		'#description' => t('List of recipients for the notification other than the customer. Enter one e-mail address per line.'),
		'#default_value' => variable_get('uc_license_keys_notification_recipients', variable_get('uc_store_email', ini_get('sendmail_from'))),
		'#rows' => 3,
	);

	return system_settings_form($form);
}

/**
 * uc_license_keys_edit
 * @param $form the form
 * @param $key_id the primary key for a license key
 * @return $form
 */
function uc_license_keys_edit($form_state, $key_id) {

	$lk = _uc_license_keys_load_key($key_id);

	if (!$lk) {
		drupal_set_message(t('This license key does not exist.'), 'error');
		$form['#redirect'] = '/node';
		return;
	}

	$products = _uc_license_keys_load_products();	

	if (count($products) <= 0) {
		drupal_set_message(t('No products have been configured. Please configure a product for your store before continuing.'), 'error');
		return;
	}

	$product_options = array();
	$product_options[0] = t('None');
	foreach($products as $product) {
		$product_options[$product->model] = $product->model;
	}

	$form['status'] = array(
		'#type' => 'radios',
		'#title' => t('Status'),
		'#description' => t('Enable or disable this license key.'),
		'#options' => array(0 => t('Unused'), 1 => t('Used')),
		'#default_value' => isset($form['post']['status']) ? $form['post']['status'] : $lk->status,
		'#required' => TRUE,
	);

	$form['license_key'] = array(
		'#type' => 'textfield',
		'#title' => t('License key'),
		'#description' => t('Modify the license key.'),
		'#default_value' => isset($form['post']['license_key']) ? $form['post']['license_key'] : $lk->license_key,
		'#required' => TRUE,
	);

	$form['sku'] = array(
		'#type' => 'select',
		'#title' => t('SKU'),
		'#description' => t('Modify the SKU for this license key.'),
		'#options' => $product_options,
		'#default_value' => isset($form['post']['sku']) ? $form['post']['sku'] : $lk->model,
		'#required' => TRUE,
	);

	$form['key_id'] = array(
		'#type' => 'value',
		'#value' => $key_id,
	);

	$form['delete'] = array(
		'#type' => 'button',
		'#value' => t('Remove'),
		'#submit' => array('uc_license_keys_delete'),
		'#weight' => 5,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);

	return $form;
}

/**
 * uc_license_keys_edit_submit callback
 * @param $form
 * @param &$form_state
 */
function uc_license_keys_edit_submit($form, &$form_state) {
	$lk = _uc_license_keys_load_key($form_state['values']['key_id']);

	if (!$lk) {
		drupal_set_message(t('This license key does not exist.'), 'error');
		$form['#redirect'] = '/node';
		return;
	}

	$lk->license_key = $form_state['values']['license_key'];
	$lk->status = $form_state['values']['status'];

	$ret = drupal_write_record('uc_license_keys', $lk, array('key_id'));
	if (!$ret) {
		drupal_set_message(t('Could not save license key.'), 'error');
	}
	drupal_set_message(t('License key saved successfully.'));
}

/**
 * uc_license_keys_delete callback
 * @param $form
 * @param &$form_state
 */
function uc_license_keys_delete($form, &$form_state) {
	$lk = _uc_license_keys_load_key($form_state['values']['key_id']);

	if (!$lk) {
		drupal_set_message(t('This license key does not exist.'), 'error');
		$form['#redirect'] = 'node';
		return;
	}

	$ret = db_query("DELETE FROM {uc_license_keys} WHERE key_id = %d", $lk->key_id);
	if (!$ret) {
		drupal_set_message(t('Could not delete license key.'), 'error');
	}
	drupal_set_message(t('Deleted license key successfully.'));
	$form['#redirect'] = 'admin/store/products/license_keys';
}
