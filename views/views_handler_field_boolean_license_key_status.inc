<?php

/**
 * views_handler_field_boolean_license_key_status class
 *
 */
class views_handler_field_boolean_license_key_status extends views_handler_field_boolean {
	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);
		$form['type']['#options']['used-unused'] = t('Used/Unused');
	}

	function render($values) {
		if ($this->options['type'] == 'used-unused') {
			$value = $values->{$this->field_alias};
			if (!empty($this->options['not'])) {
				$value = !$value;
			}
			return $value ? t('Used') : t('Unused');
		}
		else {
			return parent::render($values);
		}
	}
}
