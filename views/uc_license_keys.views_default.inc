<?php

/**
 * @file
 *		 Provides default view(s) for uc_license_keys
 *
 */

function uc_license_keys_views_default_views() {
	$views = array();

	$view = new view;
	$view->name = 'license_keys';
	$view->description = 'License keys';
	$view->tag = 'ubercart';
	$view->view_php = '';
	$view->base_table = 'uc_license_keys';
	$view->is_cacheable = FALSE;
	$view->api_version = 2;
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	$handler = $view->new_display('default', 'Defaults', 'default');
	$handler->override_option('relationships', array(
	'model' => array(
		'label' => 'Product',
		'required' => 1,
		'id' => 'model',
		'table' => 'uc_license_keys',
		'field' => 'model',
		'relationship' => 'none',
	),
	'order_id' => array(
		'label' => 'Order',
		'required' => 0,
		'id' => 'order_id',
		'table' => 'uc_license_keys',
		'field' => 'order_id',
		'relationship' => 'none',
	),
	'vid' => array(
		'label' => 'Product Node',
		'required' => 1,
		'id' => 'vid',
		'table' => 'uc_products',
		'field' => 'vid',
		'relationship' => 'model',
	),
	));
	$handler->override_option('fields', array(
	'license_key' => array(
		'label' => 'License key',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'exclude' => 0,
		'id' => 'license_key',
		'table' => 'uc_license_keys',
		'field' => 'license_key',
		'relationship' => 'none',
	),
	'key_id' => array(
		'label' => 'License key id',
		'alter' => array(
			'alter_text' => FALSE,
			'text' => '',
			'make_link' => FALSE,
			'path' => '',
			'alt' => '',
			'link_class' => '',
			'prefix' => '',
			'suffix' => '',
			'trim' => FALSE,
			'max_length' => '',
			'word_boundary' => TRUE,
			'ellipsis' => TRUE,
			'strip_tags' => FALSE,
			'html' => FALSE,
		),
		'exclude' => '',
		'id' => 'key_id',
		'table' => 'uc_license_keys',
		'field' => 'key_id',
		'relationship' => 'none',
		'uc_license_keys_operations' => '1',
	),
	'title' => array(
		'label' => 'Title',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'target' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'empty' => '',
		'hide_empty' => 0,
		'empty_zero' => 0,
		'link_to_node' => 1,
		'exclude' => 0,
		'id' => 'title',
		'table' => 'node',
		'field' => 'title',
		'relationship' => 'vid',
	),
	'model' => array(
		'label' => 'SKU',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'target' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'empty' => '',
		'hide_empty' => 0,
		'empty_zero' => 0,
		'link_to_node' => 0,
		'exclude' => 0,
		'id' => 'model',
		'table' => 'uc_products',
		'field' => 'model',
		'relationship' => 'model',
	),
	'status' => array(
		'label' => 'License key status',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'type' => 'used-unused',
		'not' => 0,
		'exclude' => 0,
		'id' => 'status',
		'table' => 'uc_license_keys',
		'field' => 'status',
		'relationship' => 'none',
	),
	'order_id' => array(
		'label' => 'Order ID',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'link_to_order' => 1,
		'exclude' => 0,
		'id' => 'order_id',
		'table' => 'uc_orders',
		'field' => 'order_id',
		'relationship' => 'order_id',
	),
	'assigned' => array(
		'label' => 'License key assigned date',
		'alter' => array(
			'alter_text' => 0,
			'text' => '',
			'make_link' => 0,
			'path' => '',
			'link_class' => '',
			'alt' => '',
			'prefix' => '',
			'suffix' => '',
			'help' => '',
			'trim' => 0,
			'max_length' => '',
			'word_boundary' => 1,
			'ellipsis' => 1,
			'strip_tags' => 0,
			'html' => 0,
		),
		'date_format' => 'time ago',
		'custom_date_format' => '',
		'exclude' => 0,
		'id' => 'assigned',
		'table' => 'uc_license_keys',
		'field' => 'assigned',
		'relationship' => 'none',
	),
	));
	$handler->override_option('sorts', array(
	'key_id' => array(
		'order' => 'ASC',
		'id' => 'key_id',
		'table' => 'uc_license_keys',
		'field' => 'key_id',
		'relationship' => 'none',
	),
	));
	$handler->override_option('filters', array(
	'model' => array(
		'operator' => 'contains',
		'value' => '',
		'group' => '0',
		'exposed' => TRUE,
		'expose' => array(
			'use_operator' => 0,
			'operator' => 'model_op',
			'identifier' => 'model',
			'label' => 'Filter by SKU',
			'optional' => 1,
			'remember' => 0,
		),
		'case' => 1,
		'id' => 'model',
		'table' => 'uc_products',
		'field' => 'model',
		'relationship' => 'model',
		'override' => array(
			'button' => 'Override',
		),
	),
	'order_id' => array(
		'operator' => '=',
		'value' => array(
			'value' => '',
			'min' => '',
			'max' => '',
		),
		'group' => '0',
		'exposed' => TRUE,
		'expose' => array(
			'use_operator' => 0,
			'operator' => 'order_id_op',
			'identifier' => 'order_id',
			'label' => 'Order ID',
			'optional' => 1,
			'remember' => 0,
		),
		'id' => 'order_id',
		'table' => 'uc_orders',
		'field' => 'order_id',
		'relationship' => 'order_id',
		'override' => array(
			'button' => 'Override',
		),
	),
	'status' => array(
		'operator' => '=',
		'value' => 'All',
		'group' => '0',
		'exposed' => TRUE,
		'expose' => array(
			'operator' => '',
			'identifier' => 'status',
			'label' => 'Status',
			'optional' => 1,
			'remember' => 0,
		),
		'id' => 'status',
		'table' => 'uc_license_keys',
		'field' => 'status',
		'relationship' => 'none',
	),
	));
	$handler->override_option('access', array(
	'type' => 'perm',
	'perm' => 'administer license keys',
	));
	$handler->override_option('cache', array(
	'type' => 'none',
	));
	$handler->override_option('items_per_page', 50);
	$handler->override_option('use_pager', '1');
	$handler->override_option('style_plugin', 'table');
	$handler->override_option('style_options', array(
	'grouping' => '',
	'override' => 1,
	'sticky' => 1,
	'order' => 'asc',
	'columns' => array(
		'license_key' => 'license_key',
		'key_id' => 'key_id',
		'title' => 'title',
		'model' => 'model',
		'status' => 'status',
		'order_id' => 'order_id',
		'assigned' => 'assigned',
	),
	'info' => array(
		'license_key' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'key_id' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'title' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'model' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'status' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'order_id' => array(
			'sortable' => 1,
			'separator' => '',
		),
		'assigned' => array(
			'sortable' => 1,
			'separator' => '',
		),
	),
	'default' => 'license_key',
	));
	$handler = $view->new_display('page', 'License keys', 'page_1');
	$handler->override_option('path', 'admin/store/products/license_keys/view');
	$handler->override_option('menu', array(
	'type' => 'default tab',
	'title' => 'View',
	'description' => 'License keys',
	'weight' => '-10',
	'name' => 'navigation',
	));
	$handler->override_option('tab_options', array(
	'type' => 'normal',
	'title' => 'License keys',
	'description' => 'License keys',
	'weight' => '5',
	));

	$views[$view->name] = $view;

	return $views;
}
