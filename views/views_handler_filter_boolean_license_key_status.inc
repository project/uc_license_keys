<?php

/**
 * views_handler_filter_boolean_license_key_status class
 *
 */
class views_handler_filter_boolean_license_key_status extends views_handler_filter_many_to_one {
	function get_value_options() {
		$this->value_options = array(0 => t('Unused'), 1 => t('Used'));
	}
}
