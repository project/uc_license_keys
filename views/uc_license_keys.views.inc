<?php

/**
 * @file
 * Views main include file for License Keys module
 */

/**
 * Implementation of hook_views_data
 */
function uc_license_keys_views_data() {
  /*
   * Implement fields that ubercart completely fails at...
   */
  $data['uc_products']['vid'] = array(
    'title' => t('Version Id'),
    'help' => t('The primary identifier for an ubercart product.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'vid',
      'relationship field' => 'vid',
      'label' => t('Product Node'),
    ),
  );


	$data['uc_license_keys']['table']['group'] = t('Ubercart License Keys');
	$data['uc_license_keys']['table']['base'] = array(
		'field' => 'key_id',
		'title' => t('License keys'),
		'help' => t('Develop a dynamic query using Ubercart License keys as the main table.'),
		'weight' => 2,
	);

	$data['uc_license_keys']['key_id'] = array(
		'title' => t('License key id'),
		'help' => t('The license key id is the primary key for license keys.'),
		'field' => array(
			'handler' => 'views_handler_field_license_key_id',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	$data['uc_license_keys']['order_id'] = array(
		'title' => t('Order id'),
		'help' => t('The Order id for the product if defined.'),
		'relationship' => array(
			'handler' => 'views_handler_relationship',
			'base' => 'uc_orders',
			'base field' => 'order_id',
			'relationship field' => 'order_id',
			'label' => t('Order')
		),
	);

	$data['uc_license_keys']['model'] = array(
		'title' => t('SKU'),
		'help' => t('The SKU for the product.'),
		'relationship' => array(
			'handler' => 'views_handler_relationship',
			'base' => 'uc_products',
			'base field' => 'model',
			'relationship field' => 'model',
			'label' => t('SKU'),
		),
	);

	$data['uc_license_keys']['license_key'] = array(
		'title' => t('License key'),
		'help' => t('This is the actual license key to display.'),
		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter',
		),
	);

	$data['uc_license_keys']['status'] = array(
		'title' => t('License key status'),
		'help' => t('This is the status of the license key.'),
		'field' => array(
			'handler' => 'views_handler_field_boolean_license_key_status',
			'click sortable' => TRUE,
			'type' => 'unused/used',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_boolean_license_key_status',
			'label' => t('Available'),
			'type' => 'unused/used',
		),
	);

	$data['uc_license_keys']['assigned'] = array(
		'title' => t('License key assigned date'),
		'help' => t('The date that the license key was assigned.'),
		'field' => array(
			'handler' => 'views_handler_field_date',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort_date',
		),
	);

	return $data;
}

/**
 * Implementation of hook_views_handlers
 */
function uc_license_keys_views_handlers() {
	return array(
		'info' => array(
			'path' => drupal_get_path('module', 'uc_license_keys') . '/views',
		),
		'handlers' => array(
			'views_handler_field_license_key_id' => array(
				'parent' => 'views_handler_field',
			),
			'views_handler_filter_boolean_license_key_status' => array(
        'parent' => 'views_handler_filter_many_to_one',
			),
			'views_handler_field_boolean_license_key_status' => array(
				'parent' => 'views_handler_field_boolean',
			),
		),
	);
}
