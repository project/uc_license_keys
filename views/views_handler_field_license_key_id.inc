<?php

/**
 * views_handler_field_license_key_id class
 *
 * @function options_form display an option to enable or disable rendering operations
 * @function theme_functions register a theme function
 * @function render render operations or not dependant on $this->options['uc_license_keys_operations']
 */
class views_handler_field_license_key_id extends views_handler_field {

	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);
		$form['uc_license_keys_operations'] = array(
			'#type' => 'radios',
			'#title' => t('Display edit link'),
			'#description' => t('Enabling this will provide a link to edit the license key.'),
			'#options' => array(0 => t('No'), 1 => t('Yes')),
			'#default_value' => isset($this->options['uc_license_keys_operations']) ? $this->options['uc_license_keys_operations'] : 1,
		);
	}

	function render($values) {
		if ($this->options['uc_license_keys_operations'] == 1) {
			$key_id = $values->{$this->field_alias};
			return l($key_id, 'admin/store/products/license_keys/edit/'.$key_id);
		}
		else {
			return $values->{$this->field_alias};
		}
	}
}
